#ifndef LDDSTATE_H
#define LDDSTATE_H
#include <set>
#include <vector>
#include <sylvan.h>
using namespace std;

typedef set<int> Set;
class LDDState
{
    public:
        LDDState(){m_boucle=m_blocage=m_visited=false;m_virtual=false;}
        virtual ~LDDState();
        Set firable;
        void * Class_Appartenance;
        vector<pair<LDDState*,int> > Predecessors, Successors;
		pair<LDDState*,int>  LastEdge;
		void setLDDValue(MDD m);
		MDD  getLDDValue();
        MDD   m_lddstate;
        char m_SHA2[80];
        char* getSHAValue();
		bool m_boucle;
		bool m_blocage;
		bool m_visited;
		bool isVirtual();
		void setVirtual();

    protected:

    private:
        bool m_virtual=false;

};

typedef pair<LDDState*, int> LDDEdge;
typedef vector<LDDEdge> LDDEdges;
#endif // LDDSTATE_H
