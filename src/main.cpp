// version 0.1
#include <time.h>
#include <chrono>
#include <iostream>
#include <string>
using namespace std;
//#include "bdd.h"
//#include "fdd.h"
#include <mpi.h>
#include "DistributedSOG.h"
#include "LDDGraph.h"
#include "Net.hpp"
#include "RdPBDD.h"
#define MASTER 0

int Formula_transitions(const char* f, Set_mot& formula_trans, net Rv);

/***********************************************/
int main(int argc, char** argv) {
  int choix;
  int b = 32;
  if (argc < 3) return 0;
  char Obs[100] = "";
  char Int[100] = "";
  if (argc > 3) strcpy(Obs, argv[2]);
  if (argc > 4) strcpy(Int, argv[3]);
  b = atoi(argv[argc - 1]);
  cout << "Fichier net : " << argv[1] << endl;
  cout << "Fichier formule : " << Obs << endl;
  cout << "Fichier Interface : " << Int << endl;
  cout << "______________________________________\n";
  net R(argv[1], Obs, Int);

  MPI_Init(&argc, &argv);
  // cout<<"______________Apres construction de net________________________\n";
  // cout<<R<<endl;
  double d, tps;
  DistributedSOG DR(R, b);
  LDDGraph g;

  // cout<<"LDD Graph construction to be started..."<<endl;
  // d = getTime();
  //  auto t1 = std::chrono::high_resolution_clock::now();
  DR.computeDSOG(g);

  /*	   auto t2 = std::chrono::high_resolution_clock::now();
         std::cout << "temps de construction du graphe d'observation "
                <<
     std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1).count()
                << " milliseconds\n"; */
  // tps = getTime() - d;
  // cout << " Temps de construction du graphe d'observation " << tps << endl;

  // g. printCompleteInformation();
  MPI_Finalize();
  return 0;
}
