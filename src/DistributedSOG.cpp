#include "DistributedSOG.h"
// #include <vec.h>

#include <stdio.h>
// #include "test_assert.h"
#include <sylvan.h>
//#include <boost/thread.hpp>
#define LENGTH_ID 80
#define TAG_STATE 123
#define TAG_SEND 232
#define TAG_REC 333
#define TAG_TERM 88






/**************************************************/

typedef struct __attribute__((packed)) mddnode
{
    uint64_t a, b;
} * mddnode_t; // 16 bytes

#define GETNODE(mdd) ((mddnode_t)llmsset_index_to_ptr(nodes, mdd))

static inline uint32_t __attribute__((unused))
mddnode_getvalue(mddnode_t n)
{
    return *(uint32_t*)((uint8_t*)n+6);
}

static inline uint64_t __attribute__((unused))
mddnode_getdown(mddnode_t n)
{
    return n->b >> 17;
}

static inline uint64_t __attribute__((unused))
mddnode_getright(mddnode_t n)
{
    return (n->a & 0x0000ffffffffffff) >> 1;
}
/*************************************************/




const vector<class Place> *_vplaces = NULL;

/*void my_error_handler_dist(int errcode) {
    cout<<"errcode = "<<errcode<<endl;
	if (errcode == BDD_RANGE) {
		// Value out of range : increase the size of the variables...
		// but which one???
		bdd_default_errhandler(errcode);
	}
	else {
		bdd_default_errhandler(errcode);
	}
} */


DistributedSOG::DistributedSOG(const net &R, int BOUND,bool init)
{


    lace_init(1, 0); // auto-detect number of workers, use a 1,000,000 size task queue

    //lace_init(1, 0);
    lace_startup(0, NULL, NULL);

    sylvan_init_package(1LL<<27, 1LL<<27, 1LL<<20, 1LL<<24);	//sylvan_init_bdd(1);
    sylvan_init_ldd();
//sylvan_gc_test();
    sylvan_gc_enable();
    ////////////////////////////////////////////////
    /*uint32_t test[3];
    test[0]=1;
    test[1]=2;
    test[2]=3;
    MDD m=lddmc_cube(test,3);
    exit(0);*/
    ////////////////////////////////////////////////

    m_net=R;

    m_init=init;
    nbPlaces=R.places.size();
    int i, domain;
    vector<Place>::const_iterator it_places;

    //_______________
    transitions=R.transitions;
    Observable=R.Observable;
    NonObservable=R.NonObservable;
    Formula_Trans=R.Formula_Trans;
    transitionName=R.transitionName;
    InterfaceTrans=R.InterfaceTrans;
    Nb_places=R.places.size();
    cout<<"Nombre de places : "<<Nb_places<<endl;
    cout<<"Derniere place : "<<R.places[Nb_places-1].name<<endl;
    // place domain, place bvect, place initial marking and place name


    uint32_t * liste_marques=new uint32_t[R.places.size()];
    for(i=0,it_places=R.places.begin(); it_places!=R.places.end(); i++,it_places++)
    {
        liste_marques[i] =it_places->marking;
    }
    M0=lddmc_cube(liste_marques,R.places.size());
    delete []liste_marques;
    // place names
    _vplaces = &R.places;


    uint32_t *prec = new uint32_t[nbPlaces];
    uint32_t *postc= new uint32_t [nbPlaces];
    // Transition relation
    for(vector<Transition>::const_iterator t=R.transitions.begin();
            t!=R.transitions.end(); t++)
    {
        // Initialisation
        for(i=0; i<nbPlaces; i++)
        {
            prec[i]=0;
            postc[i]=0;
        }
        // Calculer les places adjacentes a la transition t
        set<int> adjacentPlace;
        for(vector< pair<int,int> >::const_iterator it=t->pre.begin(); it!=t->pre.end(); it++)
        {
            adjacentPlace.insert(it->first);
            prec[it->first] = prec[it->first] + it->second;
            //printf("It first %d \n",it->first);
            //printf("In prec %d \n",prec[it->first]);
        }
        // arcs post
        for(vector< pair<int,int> >::const_iterator it=t->post.begin(); it!=t->post.end(); it++)
        {
            adjacentPlace.insert(it->first);
            postc[it->first] = postc[it->first] + it->second;
        }

        MDD _minus=lddmc_cube(prec,nbPlaces);
        MDD _plus=lddmc_cube(postc,nbPlaces);
        m_tb_relation.push_back(TransSylvan(_minus,_plus));
    }
    delete [] prec;
    delete [] postc;

}



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////// Version distribu�e en utilisant les LDD - MPI/////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


void *DistributedSOG::doCompute()
{
    int nb_it=0;
    nb_failed=0;
    nb_success=0;

    int source=0;

    /*int m_nbmetastate=0;
    int m_MaxIntBdd=0;
    int m_NbIt=0;
    int m_itext=m_itint=0;*/
    Pair S;
//    pile m_st;
    char msg[LENGTH_ID];



    int v_nbmetastate=0;
    int term=0;

    bool Terminated=false;
    bool Terminating=false;


    /****************************************** initial state ********************************************/
    if (task_id==MASTER)
    {
        cout<<":nbre de places"<<Nb_places<<endl;

        LDDState *c=new LDDState;
        MDD Complete_meta_state=Accessible_epsilon(M0);
        c->m_lddstate=Complete_meta_state;
        lddmc_getsha(Complete_meta_state, msg);
        int destination=(int)abs((msg[0])%n_tasks);


        if(destination==MASTER)
        {
            m_nbmetastate++;
            //m_old_size=lddmc_nodecount(c->m_lddstate);
            fire=firable_obs(Complete_meta_state);
            m_st.push(Pair(couple(c,Complete_meta_state),fire));
            m_graph->setInitialState(c);
            m_graph->insert(c);
            // m_graph->m_nbMarking+=lddmc_nodecount(c->m_lddstate);

        }
        else
        {
            char *message_to_send;
            unsigned int message_size;
            convert_wholemdd_string(M0,&message_to_send,message_size);
            MPI_Send(message_to_send, message_size, MPI_CHAR, destination, TAG_STATE, MPI_COMM_WORLD);
            delete []message_to_send;
            nbsend++;
            delete c;
        }
    }


//MPI_Barrier(MPI_COMM_WORLD);

    while (Terminated==false)
    {
        // cout<<"Nouvelle it�ration"<<endl;
        /****************************************** debut boucle pile ********************************************/
        if (!m_st.empty())
        {

            m_NbIt++;
            term=0;
            Pair  e=m_st.top();
            m_st.pop();

            m_nbmetastate--;
            LDDState *reached_class=NULL;
            // cout<<"debut boucle pile process "<<task_id<<endl;
            while(!e.second.empty())
            {

                //    FILE *fp=fopen("test.dot","w");
                int t = *(e.second.begin());
                e.second.erase(t);
                double nbnode;
                reached_class=new LDDState;

                //  lddmc_fprintdot(fp,Complete_meta_state);
                MDD marking=get_successorMDD(e.first.second,t);
                MDD lddmarq=Accessible_epsilon(marking);
                reached_class->m_lddstate=lddmarq;
                lddmc_getsha(lddmarq, msg);



                int destination=(int)abs((msg[0])%n_tasks);

                /**************** construction local ******/


                if(destination==task_id)
                {

                    LDDState* pos=m_graph->find(reached_class);

                    if(!pos)
                    {

                        m_graph->addArc();
                        m_graph->insert(reached_class);

                        e.first.first->Successors.insert(e.first.first->Successors.begin(),LDDEdge(reached_class,t));
                        reached_class->Predecessors.insert(reached_class->Predecessors.begin(),LDDEdge(e.first.first,t));
                        m_nbmetastate++;

                        fire=firable_obs(lddmarq);

                        m_st.push(Pair(couple(reached_class,lddmarq),fire));
                        nb_success++;


                    }
                    else
                    {
                        m_graph->addArc();
                        nb_failed++;
                        e.first.first->Successors.insert(e.first.first->Successors.begin(),LDDEdge(pos,t));
                        pos->Predecessors.insert(pos->Predecessors.begin(),LDDEdge(e.first.first,t));
                        delete reached_class;

                    }
                }
                /**************** construction externe ******/
                else // send to another process
                {

                    LDDState* posV=m_graph->findSHA(msg);

                    if(!posV)
                    {


                        m_graph->addArc();

                        m_graph->insertSHA(reached_class);

                        strcpySHA(reached_class->m_SHA2,msg);

                        e.first.first->Successors.insert(e.first.first->Successors.begin(),LDDEdge(reached_class,t));
                        reached_class->Predecessors.insert(reached_class->Predecessors.begin(),LDDEdge(e.first.first,t));

                        v_nbmetastate++;

                        /*char *message_to_send;
                        unsigned int message_size;
                        //MDD mark=Lockstep(marking);
                        convert_wholemdd_string(marking,&message_to_send,message_size);*/

                        MDD Reduced=Canonize(marking,0);
//                        MDD Reduced=marking;
                        char *message_to_send2;
                        unsigned int message_size2;
                        convert_wholemdd_string(Reduced,&message_to_send2,message_size2);
                        //cout<<"Size 1 "<<message_size<<" Size 2 : "<<message_size2<<endl;


                        //   cout<<"Message to be sent Size : "<<message_size<<" by process  :"<<task_id<<"to process"<<destination<<endl;
                        read_state_message();
                        MPI_Isend(message_to_send2, message_size2, MPI_CHAR, destination, TAG_STATE, MPI_COMM_WORLD,&m_request);
                        // MPI_Send(message_to_send, message_size, MPI_CHAR, destination, TAG_STATE, MPI_COMM_WORLD);
                        //cout<<"Message sent Size : "<<message_size<<" by process  :"<<task_id<<"to process"<<destination<<endl;
                        read_state_message();
                        nbsend++;
                        MPI_Wait(&m_request,&m_status);
                        //delete []message_to_send;
                        delete []message_to_send2;

                    }
                    else
                    {

                        m_graph->addArc();
                        e.first.first->Successors.insert(e.first.first->Successors.begin(),LDDEdge(posV,t));
                        posV->Predecessors.insert(posV->Predecessors.begin(),LDDEdge(e.first.first,t));
                        delete reached_class;
                    }






                }


            }


        }
        /****************************************** fin boucle pile ********************************************/

        read_state_message();


        int flag_s=0, flag_r=0, flag_term=0;

        /****************************************** debut reception de nombre de msg recu ********************************************/
        if(m_st.empty())
        {

            //cout<<"Reception nombre message recu"<<endl;
            MPI_Iprobe(task_id==0 ? n_tasks-1 : task_id-1,TAG_REC,MPI_COMM_WORLD,&flag_r,&m_status); // exist a msg to receiv?

            if(flag_r!=0)
                //   if(status.MPI_TAG==tag_rec)
            {
                int kkk;
                MPI_Irecv(&kkk, 1, MPI_INT, task_id==0 ? n_tasks-1 : task_id-1, TAG_REC, MPI_COMM_WORLD, &m_request);
                if(task_id!=0)
                {
                    //printf("Process %d  nb receive %d\n", task_id, kkk);
                    kkk=kkk+nbrecv;
                    MPI_Send( &kkk, 1, MPI_INT, (task_id+1)%n_tasks, TAG_REC, MPI_COMM_WORLD );
                }
                else
                {

                    total_nb_recv=kkk;
                    //printf("Process 0  total receive %d\n", total_nb_recv);
                    MPI_Send( &nbsend, 1, MPI_INT, 1, TAG_SEND, MPI_COMM_WORLD );
                    //printf("Process 0  nb send au debut %d\n", nbsend);
                }

            }
        }
        /****************************************** debut reception de nombre de msg envoy� ********************************************/
        if(m_st.empty())
        {


            MPI_Iprobe(task_id==0 ? n_tasks-1 : task_id-1,TAG_SEND,MPI_COMM_WORLD,&flag_s,&m_status); // exist a msg to receiv?

            if(flag_s!=0)
                //if(status.MPI_TAG==tag_send)
            {
                int kkk;

                MPI_Irecv( &kkk, 1, MPI_INT,task_id==0 ? n_tasks-1 : task_id-1, TAG_SEND, MPI_COMM_WORLD, &m_request);
                //  cout<<" Terminaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaate \n"<<task_id<<endl;

                if(task_id!=0)
                {
                    //printf("Process %d nb send  %d \n", task_id, nbsend);
                    kkk=kkk+nbsend;
                    MPI_Send( &kkk, 1, MPI_INT, (task_id + 1) % n_tasks, TAG_SEND, MPI_COMM_WORLD );
                }
                else if(kkk==total_nb_recv)
                {
                    MPI_Send( &term, 1, MPI_INT, (task_id + 1) % n_tasks, TAG_TERM, MPI_COMM_WORLD );

                }
                else
                {
                    Terminating=false;
                }





            }
        }

        /****************************************** debut reception de msg de terminaison ********************************************/
        if(m_st.empty())
        {


            MPI_Iprobe(task_id==0 ? n_tasks-1 : task_id-1,TAG_TERM,MPI_COMM_WORLD,&flag_term,&m_status); // exist a msg to receiv?

            if(flag_term!=0 )
                //if(status.MPI_TAG==tag_term)
            {
                MPI_Irecv(&term, 1, MPI_INT, task_id==0 ? n_tasks-1 : task_id-1, TAG_TERM, MPI_COMM_WORLD,&m_request);
                //  cout<<" Terminaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaate \n"<<task_id<<endl;
                if(task_id!=0)
                {

                    MPI_Send(&term, 1, MPI_INT, (task_id + 1) % n_tasks, TAG_TERM, MPI_COMM_WORLD);
                }
                Terminated=true;

            }

        }

        /****************************************** Master declanche la debut de terminaison*****************************/

        if(task_id==0 && Terminating==false && m_st.empty())
        {

            //cout<<" Prooooobe Termination \n"<<endl;
            Terminating=true;
            MPI_Send( &nbrecv, 1, MPI_INT, (task_id+1)%n_tasks, TAG_REC, MPI_COMM_WORLD );
            // printf("Process 0  nb receiv au debut %d\n", nbrecv);
            // cout<<" Terminaaaaaaaaaaaaaaaaaaaaaaaaaaaaaate \n"<<task_id<<endl;


        }

    }

}

void DistributedSOG::read_state_message()
{
    MPI_Status   status;
    int flag;
    flag=0;
    /****************************************** debut reception state ********************************************/
    MPI_Iprobe(MPI_ANY_SOURCE,TAG_STATE,MPI_COMM_WORLD,&flag,&status); // exist a msg to receiv?
    while(flag!=0)
    {


        int nbytes;
        MPI_Get_count(&status, MPI_CHAR, &nbytes);
        char *inmsg=new char[nbytes];
        //  cout<<"Message to be received Size : "<<nbytes<<" by process  :"<<task_id<<endl;
        MPI_Recv(inmsg, nbytes, MPI_CHAR, status.MPI_SOURCE, TAG_STATE,MPI_COMM_WORLD, &status);
        //  cout<<"Message  received Size : "<<nbytes<<" by process  :"<<task_id<<endl;
        nbrecv++;
        MDD M=decodage_message(inmsg); //lddmc_cube(l_marques,nbPlaces);
        delete []inmsg;
        // delete []l_marques;
        LDDState* Agregate=new LDDState;
        MDD MState=Accessible_epsilon(M);
        Agregate->m_lddstate=MState;

        if (!m_graph->find(Agregate))
        {
            m_graph->insert(Agregate);
            fire=firable_obs(MState);
            m_nbmetastate++;
            m_old_size=lddmc_nodecount(Agregate->m_lddstate);
            m_st.push(Pair(couple(Agregate,MState),fire));


        }
        else delete Agregate;
        MPI_Iprobe(MPI_ANY_SOURCE,TAG_STATE,MPI_COMM_WORLD,&flag,&status); // exist a msg to receiv?
        //    cout<<"Fin Boucle r�ception state "<<task_id<<endl;

    }

}



void DistributedSOG::computeDSOG(LDDGraph &g)

{

    MPI_Comm_size(MPI_COMM_WORLD,&n_tasks);
    MPI_Comm_rank(MPI_COMM_WORLD,&task_id);

    int rc;
    m_graph=&g;

    double d,tps;
    auto t1 = std::chrono::high_resolution_clock::now();

    MPI_Barrier(MPI_COMM_WORLD);


    doCompute();
    int sum_nbStates=0;
    int sum_nbArcs=0;
    int nbstate=g.m_nbStates;
    int nbarcs=g.m_nbArcs;
    MPI_Reduce(&nbstate, &sum_nbStates, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
    MPI_Reduce(&nbarcs, &sum_nbArcs, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);




    if(task_id==0)
    {
        //cout<<"NB META STATE DANS CONSTRUCTION : "<<m_nbmetastate<<endl;
        //cout<<"NB ITERATIONS CONSTRUCTION : "<<m_NbIt<<endl;
        //cout<<"Nb Iteration externes : "<<m_itext<<endl;
        // cout<<"Nb Iteration internes : "<<m_itint<<endl;*/
        auto t2 = std::chrono::high_resolution_clock::now();
        std::cout << "temps de construction du graphe d'observation "
                  << std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1).count()
                  << " milliseconds\n";

        cout<<"NB META-STATE****** "<<sum_nbStates<<endl;
        cout<<"NB ARCS****** "<<sum_nbArcs<<endl;
    }

}



void DistributedSOG::printhandler(ostream &o, int var)
{
    o << (*_vplaces)[var/2].name;
    if (var%2)
        o << "_p";
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////// /////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Set DistributedSOG::firable_obs(MDD State)
{
    Set res;
    for(Set::const_iterator i=Observable.begin(); !(i==Observable.end()); i++)
    {

        //cout<<"firable..."<<endl;
        MDD succ = lddmc_firing_mono(State,m_tb_relation[(*i)].getMinus(),m_tb_relation[(*i)].getPlus());
        if(succ!=lddmc_false)
        {
            //cout<<"firable..."<<endl;
            res.insert(*i);
        }
    }
    return res;
}

MDD DistributedSOG::get_successorMDD(MDD From,int t)
{

    MDD res=lddmc_firing_mono(From,m_tb_relation[(t)].getMinus(),m_tb_relation[(t)].getPlus());
    return res;
}


MDD DistributedSOG::Accessible_epsilon(MDD From)
{
    MDD M1;
    MDD M2=From;
    int it=0;
    do
    {
        M1=M2;
        for(Set::const_iterator i=NonObservable.begin(); !(i==NonObservable.end()); i++)
        {

            MDD succ= lddmc_firing_mono(M2,m_tb_relation[(*i)].getMinus(),m_tb_relation[(*i)].getPlus());
            M2=lddmc_union_mono(M2,succ);
            //M2=succ|M2;
        }
        //TabMeta[nbmetastate]=M2;
        //int intsize=sylvan_anodecount(TabMeta,nbmetastate+1);
        //if(m_MaxIntBdd<intsize)
        //  m_MaxIntBdd=intsize;
        it++;
        //	cout << bdd_nodecount(M2) << endl;
    }
    while(M1!=M2);
    return M2;
}


string to_hex(unsigned char s)
{
    stringstream ss;
    ss << hex << (int) s;
    return ss.str();
}




char * DistributedSOG::concat_string(const char *s1,int longueur1,const char *s2,int longueur2,char *dest)
{
    for (int i=0; i<longueur1; i++)
    {
        dest[i]=(char)(s1[i]);
    }
    for (int i=longueur1; i<longueur1+longueur2; i++)
        dest[i]=s2[i-longueur1];
    dest[longueur1+longueur2]='\0';
    return dest;
}


void DistributedSOG::strcpySHA(char *dest,const char *source)
{
    for (int i=0; i<LENGTH_ID; i++)
    {
        dest[i]=source[i];
    }
    dest[LENGTH_ID]='\0';
}



DistributedSOG::~DistributedSOG()
{
    //dtor
}

/******************************************convert MDD ---> char ********************************************/

void DistributedSOG::convert_wholemdd_string(MDD cmark,char **result,unsigned int &length)
{

    typedef pair<string,MDD> Pair_stack;
    vector<Pair_stack> local_stack;
    unsigned int indice=0;
    unsigned int count=0;
    MDD explore_mdd=cmark;
    string res;
    string chaine="";
    local_stack.push_back(Pair_stack(chaine,cmark));
    do
    {
        Pair_stack element=local_stack.back();
        chaine=element.first;
        explore_mdd=element.second;
        local_stack.pop_back();
        count++;
        while ( explore_mdd!= lddmc_false && explore_mdd!= lddmc_true )
        {
            mddnode_t n_val = GETNODE(explore_mdd);
            if (mddnode_getright(n_val)!=lddmc_false)
            {

                local_stack.push_back(Pair_stack(chaine,mddnode_getright(n_val)));
            }
            unsigned int val = mddnode_getvalue(n_val);
            chaine.push_back(char(val));
            explore_mdd=mddnode_getdown(n_val);
        }
        res+=chaine;
    }
    while (local_stack.size()!=0);
    *result= new char[res.size()+2];
    **result=(unsigned char)count;
    *(*result+1)=(unsigned char )(count>>8);
    for (unsigned int i=2; i<res.size()+2; i++)
        *(*result+i)=res.at(i-2);
    //*(*result+res.size()+2)='\0';
    length=res.size()+2;
    //  cout<<" Count : "<<count<<"by process "<<task_id<<endl;
}

/******************************************convert char ---> MDD ********************************************/

MDD DistributedSOG::decodage_message(char *chaine)
{
    MDD M=lddmc_false;
    unsigned int nb_marq=(unsigned char)chaine[1];
    nb_marq=(nb_marq<<8) | (unsigned char)chaine[0];

    unsigned int index=2;
    uint32_t list_marq[nbPlaces];
    for (unsigned int i=0; i<nb_marq; i++)
    {
        for (unsigned int j=0; j<nbPlaces; j++)
        {
            list_marq[j]=chaine[index];
            index++;
        }
        MDD N=lddmc_cube(list_marq,nbPlaces);
        M=lddmc_union_mono(M,N);
    }
    return M;
}


void DistributedSOG::MarquageString(char *dest,const char *source)
{
    int length=source[0];
    printf("Longueur %d\n",source[0]);
    int index=1;
    for (int nb=0; nb<length; nb++)
    {
        for (int i=0; i<nbPlaces; i++)
        {
            dest[index]=(unsigned char)source[index]+(unsigned char)'0';
            printf("%d",source[index]);
            index++;
        }
    }
    printf("\n");
    dest[index]='\0';
}

/*

MDD DistributedSOG::ImageForward(MDD From)
{
    MDD Res=From;
    // MDD succ;
    for(Set::const_iterator i=NonObservable.begin(); !(i==NonObservable.end()); i++)
    {
        MDD succ= lddmc_firing_mono(Res,m_tb_relation[(*i)].getMinus(),m_tb_relation[(*i)].getPlus());
        Res=lddmc_union_mono(Res,succ);
    }
    return Res;
}
*/




/*----------------------------------------------CanonizeR()------------------------------------*/
/* MDD DistributedSOG::Canonize(MDD s,unsigned int level)
{
    LACE_ME;
    MDD s1,s2;
    MDD Rep=lddmc_false;
        do
   		{
        ldd_divide(s, level, &s1, &s2);

        if((!(s1==lddmc_false))&&(!(s2==lddmc_false)))
        {
            MDD Front = s1;
            MDD reached = s1;
            do
            {
                //cout<<"premiere boucle interne \n";
                Front=lddmc_minus(ImageForward(Front),reached);
                reached = lddmc_union(reached,Front);
                s2 = lddmc_minus(s2,Front);
            }
            while((!(Front==lddmc_false))&&(!(s2==lddmc_false)));
        }
        if((!(s1==lddmc_false))&&(!(s2==lddmc_false)))
        {
            MDD Front=s2;
            MDD reached = s2;
            do
            {
                // cout<<"deuxieme boucle interne \n";
                Front=lddmc_minus(ImageForward(Front),reached);
                reached = lddmc_union(reached,Front);
                s1 = lddmc_minus(s1,Front);
            }
            while((!(Front==lddmc_false))&&(!(s1==lddmc_false)));
        }
        s=lddmc_union_mono(s1,s2);
	level++;

  }while((level<Nb_places) &&((s1==lddmc_false)||(s2==lddmc_false)));

    if(level>=Nb_places)
   {
     //cout<<"____________oooooooppppppppsssssssss________\n";
       return s;
   }
  else
    {
      //cout<<"________________p a s o o o p p p s s s ______\n";
      return(lddmc_union(Canonize(s1,level+1),Canonize(s2,0)));
    }
} */




/******************************************************************************/
MDD DistributedSOG::ImageForward(MDD From)
{
    MDD Res=lddmc_false;
    for(Set::const_iterator i=NonObservable.begin(); !(i==NonObservable.end()); i++)
    {
        MDD succ= lddmc_firing_mono(From,m_tb_relation[(*i)].getMinus(),m_tb_relation[(*i)].getPlus());
         Res=lddmc_union_mono(Res,succ);
    }
    return Res;
}


/*----------------------------------------------CanonizeR()------------------------------------*/
MDD DistributedSOG::Canonize(MDD s,int level)
{

    if (level>=nbPlaces || s==lddmc_false)
        return lddmc_false;
    //if (s==lddmc_true) return lddmc_true;

    MDD s0=lddmc_false,s1=lddmc_false;

    bool res=false;
    do
    {
        if (get_mddnbr(s,level)>1)
        {
            s0=ldd_divide_rec(s,level);
            s1=ldd_minus(s,s0);
            res=true;
        }
        else
        level++;
    }
    while(level<nbPlaces && !res);
    if (!res) return s;


        if (s0==lddmc_false && s1==lddmc_false) return lddmc_false;
            // if (level==nbPlaces) return lddmc_false;
            MDD Front,Reach;
            if (s0!=lddmc_false && s1!=lddmc_false)
            {
                Front=s1;
                Reach=s1;
                do
                {
                    // cout<<"premiere boucle interne \n";
                    Front=ldd_minus(ImageForward(Front),Reach);
                    Reach = lddmc_union_mono(Reach,Front);
                    s0 = ldd_minus(s0,Front);
                }
                while((Front!=lddmc_false)&&(s0!=lddmc_false));
            }
            if((s0!=lddmc_false)&&(s1!=lddmc_false))
            {
                Front=s0;
                Reach = s0;
                do
                {
                    //  cout<<"deuxieme boucle interne \n";
                    Front=ldd_minus(ImageForward(Front),Reach);
                    Reach = lddmc_union_mono(Reach,Front);
                    s1 = ldd_minus(s1,Front);
                }
                while( Front!=lddmc_false && s1!=lddmc_false );
            }



        MDD Repr=lddmc_false;

        if (isSingleMDD(s0))
        {
              Repr=s0;
        }
        else
            Repr=Canonize(s0,level);
        if (isSingleMDD(s1))
            Repr=lddmc_union_mono(Repr,s1);
        else
            Repr=lddmc_union_mono(Repr,Canonize(s1,level));
        return Repr;

    }


/* int  DistributedSOG::hash(string s)
{
	int h = 0;

	// �tape 1 - Hachage
	for(int i = 0 ; i < s.length() ; i++) // On parcourt tous les caract�res
		h += (int) s[i];
}*/
/******************************Lockstep****************************************************/

/*MDD DistributedSOG::Lockstep(MDD p)
{
     LACE_ME;
    if(p==lddmc_false) return 0;
    MDD s=lddmc_pick_cube(p);
    MDD a=0;
    MDD f=0;
    MDD b=0;
    MDD c=0;
    MDD v;
    MDD f1=lddmc_intersect(ImageForward(s),p);
    MDD b1=lddmc_intersect(ImageBackward(s),p);
    do
    {
        f=lddmc_union_mono(f,f1);
        b=lddmc_union_mono(b,b1);
        f1=ldd_minus(lddmc_intersect(ImageForward(f1),p),f);
    }
    while((!(f1==lddmc_false))&&(!(b1==lddmc_false)));

    if(f1==lddmc_false)
    {
        v=f;
        do
        {
            b1=ldd_minus(lddmc_intersect(ImageBackward(b1),p),b);
            b=lddmc_union_mono(b,b1);

        }
        while (!(lddmc_intersect(b1,f)==lddmc_false));
    }
    else
    {

        v=b;
        do
        {
            f1=ldd_minus(lddmc_intersect(ImageForward(f1),p),f);
            f=lddmc_union_mono(f,f1);

        }
        while(!(lddmc_intersect(f1,b)==lddmc_false));
    }

    if (!(lddmc_intersect(f,b)==lddmc_false))
    {
        c=lddmc_intersect(f,b);
    }

    a=lddmc_union_mono(c,lddmc_union_mono(Lockstep(lddmc_minus(v,c)),Lockstep(ldd_minus(p,lddmc_union_mono(v,s)))));
    return a;
}*/
